package window;

import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class PlayButton extends Button {
    private final ImageView playIcon;
    private final ImageView pauseIcon;

    private boolean paused;

    public PlayButton(Scene scene, String playIconPath, String pauseIconPath, boolean paused) {

        playIcon = new ImageView(new Image("file:res/"+playIconPath));
        pauseIcon = new ImageView(new Image("file:res/"+pauseIconPath));

        setStyle("-fx-background-color: transparent;");

        this.paused = paused;

        setMinSize(0, 0);

        playIcon.setPreserveRatio(true);
        pauseIcon.setPreserveRatio(true);

        setOnAction(event -> {
            toggle();
            scene.togglePause();
        });


        playIcon.fitHeightProperty().bind(heightProperty());
        playIcon.fitWidthProperty().bind(widthProperty());
        pauseIcon.fitHeightProperty().bind(heightProperty());
        pauseIcon.fitWidthProperty().bind(widthProperty());


        if (!paused) super.setGraphic(pauseIcon);
        else super.setGraphic(playIcon);
    }

    public void toggle() {
        paused = !paused;
        super.setGraphic((paused ? playIcon : pauseIcon));
    }

    public boolean isPaused() { return paused; }


}
