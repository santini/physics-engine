package window;

import javafx.scene.image.ImageView;
import renderer.Camera;

public interface Scene {

    void togglePause();

    Camera getCamera();

    ImageView getSceneImage();

}
