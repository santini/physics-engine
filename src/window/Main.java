package window;

import engine.Engine;
import engine.Projectile;
import javafx.animation.AnimationTimer;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.application.Application;
import math.Vec3;
import renderer.Camera;
import renderer.Renderer;
import renderer.objects.Shape;
import renderer.objects.Sphere;
import window.handlers.KeyHandler;

import java.util.Arrays;

import static javafx.scene.paint.Color.WHITE;


public class Main extends Application implements window.Scene {

    private final Text FPSCounter;
    private final KeyHandler keyboardHandler;

    private final Renderer renderer;
    private final Engine engine;

    private final ImageView image;
    private int IMAGE_HEIGHT = 720;
    private int IMAGE_WIDTH = 1080;
    private TimeBar timeBar;

    private final Camera camera;


    private AnimationTimer updater;
    private final double[] frameTimes = new double[50];
    private long lastFrameTime = 0;
    private int frameTimeIndex = 0 ;
    private static final float NANOS_IN_SEC = 1e-9f;
    private static final float NANOS_IN_MILISEC = 1e-6f;

    public static void main(String[] args) { launch(args); }

    public Main() {
        image = new ImageView();
        camera = new Camera(new Vec3(0, -5, 1), new Vec3(0, 1, 0));

        //ObjWrapper.createEncodeRefFile();

        renderer = new Renderer(IMAGE_HEIGHT, IMAGE_WIDTH, camera, image);


        FPSCounter = new Text(20, 20, "Here will be the FPS");
        FPSCounter.setFont(Font.font("verdana", FontWeight.LIGHT, FontPosture.REGULAR, 20));
        FPSCounter.setStroke(WHITE);
        keyboardHandler = new KeyHandler(this);

        engine = new Engine();

        Shape moonShape = new Sphere(new Vec3(0, 2, 0), 0.5f, Color.GREY);
        Projectile moonProj = new Projectile(engine, new Vec3(0, 2, 0), new Vec3(0.5f, 0, 0.1f), 10, moonShape);

        Shape earthShape = new Sphere(new Vec3(), 1, Color.BLUE);
        Projectile earthProj = new Projectile(engine, new Vec3(), new Vec3(), 5.9e9f, earthShape);

        engine.addProj(moonProj);
        engine.addProj(earthProj);


        renderer.addDrawable(moonProj.getShape());
        renderer.addDrawable(earthProj.getShape());
    }


    @Override
    public void start(Stage stage) {
        stage.setTitle("Sphere shaders");
        stage.setMinWidth(300);
        stage.setMinHeight(200);

        renderer.start();

        lastFrameTime = System.nanoTime();
        updater = new AnimationTimer() {
            @Override
            public void handle(long now) {
                frameTimeIndex = (frameTimeIndex + 1) % frameTimes.length;
                long dt = now - lastFrameTime;
                frameTimes[frameTimeIndex] = dt*NANOS_IN_MILISEC;

                float deltaTime = dt * NANOS_IN_SEC;
                keyboardHandler.update(deltaTime);
                engine.act(deltaTime * timeBar.getTimeFactor());
                renderer.render();

                FPSCounter.setText("Average frame time: " + String.format("%.2f", getAvgFrametime()) + "ms");

                lastFrameTime = System.nanoTime();
            }
        };


        Group root = new Group();


        timeBar = new TimeBar(this, 200, false, new float[]{0.1f, 0.5f, 1, 2});
        timeBar.setLayoutX(16/20d * IMAGE_WIDTH);
        timeBar.setLayoutY(1/60d * IMAGE_HEIGHT);

        root.getChildren().add(image);
        root.getChildren().add(FPSCounter);
        root.getChildren().add(timeBar);


        stage.setResizable(true);
        stage.widthProperty().addListener((event, oldVal, newVal) -> {
            //renderer.resize(IMAGE_HEIGHT, newVal.doubleValue());
            IMAGE_WIDTH = newVal.intValue();
        });
        stage.heightProperty().addListener((event, oldVal, newVal) -> {
            //renderer.resize(newVal.doubleValue(), IMAGE_WIDTH);
            IMAGE_HEIGHT = newVal.intValue();
        });

        stage.setOnCloseRequest(we -> renderer.end());

        renderer.render();



        Scene scene = new Scene(root, IMAGE_WIDTH, IMAGE_HEIGHT);
        scene.setOnKeyPressed(keyboardHandler);

        stage.setScene(scene);

        if (!timeBar.isPaused())
            updater.start();

        stage.show();

    }

    @Override
    public Camera getCamera() { return camera; }

    @Override
    public ImageView getSceneImage() { return image; }

    public void reset() {

        renderer.clearDrawables();

        Shape projShape = new Sphere(new Vec3(0, 2, 0), 0.5f, Color.GREY);
        Projectile proj1 = new Projectile(engine, new Vec3(0, 2, 0), new Vec3(0.5f, 0, 0.1f), 10, projShape, false);

        renderer.addDrawable(new Sphere(new Vec3(0, 0, 0), 1, Color.BLUE));
        renderer.addDrawable(proj1.getShape());

        renderer.render();
    }


    @Override
    public void togglePause() {
        if (timeBar.isPaused()) updater.stop();
        else {
            lastFrameTime = System.nanoTime();
            updater.start();
        }
    }

    private double getAvgFrametime() {
        return Arrays.stream(frameTimes).average().getAsDouble();
    }
}