package window;

import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;


public class TimeBar extends Group {

    private static final double ASPECT_RATIO = 2;


    private final Button back;
    private final PlayButton playButton;
    private final Button forward;

    private float timeFactor = 1;
    private int timeIndex;

    public TimeBar(Scene scene, int diag, boolean paused, float[] factors) {
        timeIndex = checkList(factors);

        final double totalHeight = diag / Math.sqrt(ASPECT_RATIO*ASPECT_RATIO + 1);
        final double totalWidth = diag * ASPECT_RATIO / Math.sqrt(ASPECT_RATIO * ASPECT_RATIO + 1);

        setLayoutY(totalHeight);
        setLayoutX(totalWidth);


        playButton = new PlayButton(scene, "play-icon.png", "pause-icon.png", paused);
        back = new Button();
        setButtonGraphics(back, "file:res/fb.png");
        forward = new Button();
        setButtonGraphics(forward, "file:res/ff.png");

        Text text = new Text("Time factor : 1.0");
        text.setStroke(Color.RED);

        double widthForAll = (totalWidth - 10)/3d;
        double heightForAll = totalHeight - 2;

        playButton.setPrefSize(widthForAll, heightForAll);
        back.setPrefSize(widthForAll, heightForAll);
        forward.setPrefSize(widthForAll, heightForAll);

        back.setLayoutX(widthForAll/2);
        back.setLayoutY(heightForAll/2);
        playButton.setLayoutX(totalWidth/2);
        playButton.setLayoutY(heightForAll/2);
        forward.setLayoutX(totalWidth - widthForAll/2);
        forward.setLayoutY(heightForAll/2);

        text.setLayoutX(totalWidth/2);
        text.setLayoutY(1.5*totalHeight);

        back.setOnAction(event -> {
            if (timeIndex > 0) {
                timeIndex--;
                timeFactor = factors[timeIndex];
                text.setText("Time factor : " + timeFactor);
            }
        });

        forward.setOnAction(event -> {
            if (timeIndex < factors.length - 1) {
                timeIndex++;
                timeFactor = factors[timeIndex];
                text.setText("Time factor : " + timeFactor);
            }
        });

        getChildren().add(back);
        getChildren().add(playButton);
        getChildren().add(forward);
        getChildren().add(text);

    }


    public float getTimeFactor() { return timeFactor; }

    public boolean isPaused() { return playButton.isPaused(); }


    private void setButtonGraphics(Button button, String url) {
        ImageView im = new ImageView(new Image(url));
        im.fitHeightProperty().bind(button.heightProperty());
        im.fitWidthProperty().bind(button.widthProperty());

        button.setStyle("-fx-background-color: transparent;");

        im.setPreserveRatio(true);

        button.setMinSize(0, 0);

        button.setGraphic(im);
    }

    private int checkList(float[] factors) {
        if (factors == null || factors.length == 0) throw new IllegalArgumentException("Empty time factors");

        int tempIndex = -1;
        for (int i = 0; i < factors.length-1; ++i) {
            if (factors[i]>=factors[i+1])
                throw new IllegalArgumentException("Factors are not ordered or two factors are the same");

            if (factors[i] == 1)  tempIndex = i;

        }

        if (tempIndex == -1) {
            if ( factors[factors.length-1] != 1)
                throw new IllegalArgumentException("The time list does not contains a '1' element.");
            else tempIndex = factors.length - 1;
        }

        return tempIndex;

    }


}

