package window.handlers;

import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import math.Vec2;
import renderer.Camera;
import window.Scene;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import static javafx.scene.input.KeyCode.*;

/**
 * Keyboard input handler
 */
public class KeyHandler implements EventHandler<KeyEvent> {
    /** Set of keys corresponding to a movement change */
    private static final Set<KeyCode> MOVEMENT_KEYS = Set.of(W, A, S, D, Q, E);

    /** Registered keys for next update */
    private final Set<KeyCode> keys = new HashSet<>();

    /** Scene of the key handler */
    private final Scene scene;

    /** Record of the mouse drag distance */
    private Vec2 dragAction = new Vec2();

    /**
     * Public constructor for the key handler
     * @param scene (Scene) Scene to act on
     */
    public KeyHandler(Scene scene) {
        this.scene = scene;
        setDragAction(scene.getSceneImage());
    }

    /**
     * Handles input events
     * @param event (KeyEvent) the event which occurred
     */
    @Override
    public void handle(KeyEvent event) {
        KeyCode key = event.getCode();
        if (MOVEMENT_KEYS.contains(key))
            keys.add(key);
    }

    /**
     * Updates the scene with the registered inputs
     * @param dt (float) Time step since last update
     */
    public void update(float dt) {
        Camera camera = scene.getCamera();
        for (KeyCode key : keys) {
            switch (key) {
                case W -> camera.moveForwards(dt);
                case A -> camera.moveLeft(dt);
                case S -> camera.moveBackwards(dt);
                case D -> camera.moveRight(dt);
                case Q -> camera.moveDown(dt);
                case E -> camera.moveUp(dt);
            }
        }

        camera.turn(-dragAction.x(), dragAction.y(), dt);

        keys.clear();
        dragAction = new Vec2();
    }

    /**
     * Sets the drag actions on the image
     * @param image (ImageView) Image to apply drag from
     */
    private void setDragAction(ImageView image) {
        AtomicReference<Point2D> dragPoint = new AtomicReference<>();

        image.setOnMousePressed(e -> dragPoint.set(new Point2D(e.getX(), e.getY())));

        image.setOnDragEntered(event -> image.setCursor(Cursor.NONE));
        image.setOnDragExited(event -> image.setCursor(Cursor.DEFAULT));

        image.setOnMouseDragged(e -> {
            dragAction.addLocal(
                    (float) (dragPoint.get().getX() - e.getX()),
                    (float) (dragPoint.get().getY() - e.getY())
            );
            dragPoint.set(new Point2D(e.getX(), e.getY()));
        });

    }

}
