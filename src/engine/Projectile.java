package engine;

import javafx.beans.property.SimpleFloatProperty;
import math.FunTimePosSpeed;
import math.Integrators.Integrateur;
import math.Integrators.RK4;
import math.Vec3;
import renderer.objects.Shape;

/**
 * Class representing a projectile in classical physics
 */
public class Projectile {


    private Vec3 position;
    private Vec3 speed;
    private Vec3 acceleration;
    private final boolean locked;

    private final SimpleFloatProperty mass;

    private final Shape shape;
    private final float dragCoef;

    private final Integrateur integrateur;
    private final Engine engine;


    /**
     * Constructor for the projectile
     * @param engine (Engine) engine to witch the projectile belongs
     * @param position (Vec3) Initial position of the projectile
     * @param speed (Vec3) Initial speed of the projectile
     * @param mass (float) Mass of the projectile
     * @param shape (Shape) Shape of the projectile
     * @param locked (boolean) indicates if the object is in a locked position in the engine
     */
    public Projectile(Engine engine, Vec3 position, Vec3 speed, float mass, Shape shape,  boolean locked) {
        this.engine = engine;
        this.position = position;
        this.speed = (locked ? new Vec3() : speed);
        this.acceleration = new Vec3();
        this.locked = locked;

        this.mass = new SimpleFloatProperty(mass);

        this.shape = shape;

        switch (shape.getCode()) {
            case SPHERE -> dragCoef = 0.47f;
            case PLANE -> throw new IllegalArgumentException("The projectile cannot be a plane");
            default -> throw new IllegalArgumentException("Shape not supported");
        }

        integrateur = new RK4(this);
    }

    public Projectile(Engine engine, Vec3 position, Vec3 speed, float mass, Shape shape) {
        this(engine, position, speed, mass, shape, false); }

    /**
     * Acts on the projectile
     * @param t (float) time at the call of the method
     * @param dt (float) Time elapsed since last act
     */
    public void act(float t, float dt) {
        if (!locked) {
            integrateur.move(getAccelerationEquation(), t, dt);
            acceleration = getAccelerationEquation().apply(t, position, speed);
        }
    }


    /**
     * Getter for the position vector of the projectile
     * @return (Vec3) Position Vector
     */
    public Vec3 getPosition() { return position.clone(); }


    /**
     * Getter for the speed vector of the projectile
     * @return (Vec3) Speed Vector
     */
    public Vec3 getSpeed() { return speed.clone(); }


    /**
     * Getter for the Acceleration vector of the projectile at a given moment
     * @return (Vec3) Acceleration Vector
     */
    public Vec3 getAcceleration() { return acceleration.clone(); }


    /**
     * Getter for the movement equations of the projectile
     * @return (FunTimePosSpeed) Function of time, position and speed describing the movement of the object
     */
    public FunTimePosSpeed getAccelerationEquation() {
        return (float t, Vec3 pos, Vec3 speed) -> FunTimePosSpeed.add(engine.getForces(this)).scale(1f/mass.get()).apply(t, pos, speed);
    }


    /**
     * Getter for the mass
     * @return (float) Mass of the object
     */
    public float getMass() { return mass.get(); }


    /**
     * Getter for the friction coefficient of the object
     * @return (float) Friction coefficient of the projectile
     */
    public float getFrictionCoef() { return 0.5f * dragCoef * 1.184f * area(); }


    private float area() {
        return switch (shape.getCode()) {
            case SPHERE -> (float) (2 * Math.PI * shape.getAttributes()[0]*shape.getAttributes()[0]);
            case PLANE -> throw new RuntimeException("Shape of projectile is a plane");
        };
    }


    /**
     * Setter for the speed of the object
     * @param speed The new speed of the object.
     */
    public void setSpeed(Vec3 speed) { this.speed = speed; }


    /**
     * Setter for the position of the object
     * @param position The new position of the object.
     */
    public void setPosition(Vec3 position) {
        this.position = position;
        shape.setPosition(position);
    }


    /**
     * Getter for the shape of the projectile
     * @return (Shape) shape of the projectile
     */
    public Shape getShape() {
        return shape;
    }
}
