package engine;

import javafx.beans.property.SimpleFloatProperty;
import math.FunTimePosSpeed;
import math.Vec3;

import java.util.ArrayList;
import java.util.List;


public class Engine {
    public static final float G = 6.67408e-11f ;
    SimpleFloatProperty earthM = new SimpleFloatProperty(5.9e9f); // Earth: 5.9e24f kg
    SimpleFloatProperty earthR = new SimpleFloatProperty(1f);     // Earth: 6371e3f m


    private float t = 0f;
    private final List<Projectile> projectileList = new ArrayList<>();


    public FunTimePosSpeed[] getForces(Projectile proj) {
        FunTimePosSpeed[] tabForces = new FunTimePosSpeed[1];
        //tabForces[0] = (t, pos, speed) -> new Vec3(0, 0, -9.81f).scaleLocal(proj.getMass());

        tabForces[0] = (t, pos, speed) -> {
            Vec3 grav = new Vec3();
            for (Projectile projectile : projectileList) {
                Vec3 r = Vec3.sub(projectile.getPosition(), pos);
                if (!r.isNull())
                    grav = Vec3.add(grav, Vec3.scale(G * projectile.getMass() * proj.getMass() / r.squaredNorm(), r.normalized()));
            }
            return grav;
        };
        //tabForces[1] = (t, pos, speed) -> Vec3.scale(proj.getFrictionCoef(), speed.negate());
        return tabForces;
    }

    public void act(float dt) {
        for (Projectile projectile : projectileList) {
            projectile.act(t, dt);
        }

        t+=dt;
    }


    public void addProj(Projectile proj) { projectileList.add(proj); }

}
