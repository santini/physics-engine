package math;

public class Vec3 {

    private static final float EPSILON = 0.001f;
    private float x;
    private float y;
    private float z;

    // ===============================================================
    // ========================= CONSTRUCTORS ========================
    // ===============================================================

    /**
     * Constructor for Vec3
     * @param x (float) x-Coordinate
     * @param y (float) y-Coordinate
     * @param z (float) z-Coordinate
     */
    public Vec3(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Copy constructor for Vec3
     * @param other (Vec3) Vector to copy
     */
    public Vec3(Vec3 other) { this(other.x, other.y, other.z);}

    /**
     * Default constructor for Vec3, creates a null vector
     */
    public Vec3() { this(0, 0, 0); }


    // ===============================================================
    // ========================= OPERATORS ===========================
    // ===============================================================


    /**
     * Adds another vector to the instance
     * @param other (Vec3) vector to add
     * @return (Vec3) The modified instance
     */
    public Vec3 addLocal(Vec3 other) {
        x += other.x;
        y += other.y;
        z += other.z;
        return this;
    }


    /**
     * Scales the instance locally
     * @param scalar (float) Scalar to scale with
     * @return (Vec3) The modified instance
     */
    public Vec3 scaleLocal(float scalar) {
        x *= scalar;
        y *= scalar;
        z *= scalar;
        return this;
    }


    /**
     * Returns the sum of both vectors
     * @param v1 (Vec3) first vector
     * @param v2 (Vec3) second vector
     * @return (Vec3) Result of the operation
     */
    public static Vec3 add(Vec3 v1, Vec3 v2) {
        return new Vec3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z); }


    /**
     * Returns the subtraction of both vectors
     * @param v1 (Vec3) first vector
     * @param v2 (Vec3) second vector
     * @return (Vec3) Result of the operation
     */
    public static Vec3 sub(Vec3 v1, Vec3 v2) {
        return new Vec3(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z); }


    /**
     * Returns the scaled vector
     * @param scalar (float) Scalar
     * @param vec (Vec3) Vector to scale
     * @return (Vec3) Result of the operation
     */
    public static Vec3 scale(float scalar, Vec3 vec) {
        return new Vec3(vec.x * scalar, vec.y * scalar, vec.z * scalar); }


    /**
     * Returns the first vector scaled added to the second one
     * @param s (float) Scalar for the first vector
     * @param vec1 (Vec3) First vector
     * @param vec2 (Vec3) Second vector
     * @return (Vec3) Result of the operation
     */
    public static Vec3 fma(float s, Vec3 vec1, Vec3 vec2) {
        return Vec3.add(Vec3.scale(s, vec1), vec2); }


    /**
     * Returns the dot product between the two vectors
     * @param v1 (Vec3) First vector
     * @param v2 (Vec3) Second vector
     * @return (float) Dot product of both vectors
     */
    public static float dot(Vec3 v1, Vec3 v2) {
        return v1.x*v2.x + v1.y*v2.y + v1.z*v2.z;
    }


    /**
     * Returns the cross product of the first vector with the second one
     * @param v1 (Vec3) First vector
     * @param v2 (Vec3) Second vector
     * @return (Vec3) v1 x v2
     */
    public static Vec3 cross(Vec3 v1, Vec3 v2) {
        return new Vec3(
                v1.y*v2.z- v2.y*v1.z,
                v2.x*v1.z-v2.z*v1.x,
                v1.x*v2.y - v1.y*v2.x);
    }


    /**
     * Rotates the vector by side radians on the horizontal plane and up on it's vertical plane
     * @param side (float) angle to turn around horizontally in radians
     * @param up (float) angle to rotate around vertically in radians
     * @return (Vec3) the instance operated on
     */
    public Vec3 rotate(float side, float up) {

        if (side != 0) {
            float cos = (float) Math.cos(side);
            float sin = (float) Math.sin(side);
            float x_ = x*cos + y*sin;
            y = y*cos - x*sin;
            x = x_;
        }

        if (up != 0) {
            float cos = (float) Math.cos(up);
            Vec3 rotateAround = new Vec3(y, -x, 0);

            rotateAround.normalize();
            Vec3 temp = this.clone();
            temp = temp.scaleLocal(cos)
                   .addLocal(Vec3.cross(rotateAround, temp).scaleLocal((float) Math.sin(up)))
                   .addLocal(rotateAround.scaleLocal(Vec3.dot(temp, rotateAround) * (1 - cos))); // Rodrigues' rotation formula

            if (Math.sqrt(temp.x*temp.x + temp.y*temp.y)<0.1) return this;

            x = temp.x;
            y = temp.y;
            z = temp.z;
        }

        return this;
    }


    // ===============================================================
    // ========================== METHODS ============================
    // ===============================================================

    public float squaredNorm() { return x*x + y*y + z*z; }

    public float norm() { return (float) Math.sqrt(squaredNorm()); }

    public void normalize() { this.scaleLocal(1/norm()); }

    public Vec3 normalized() { return Vec3.scale(1f/this.norm(), new Vec3(this));}

    public boolean isNull() { return norm()<EPSILON; }

    public float[] toArray() { return new float[]{x, y, z}; }

    public Vec3 negate() { return new Vec3(-x, -y, -z); }

    // ===============================================================
    // ========================= GETTERS =============================
    // ===============================================================

    public float x() { return x; }
    public float y() { return y; }
    public float z() { return z; }


    // ===============================================================
    // ========================= OVERRIDES ===========================
    // ===============================================================

    @Override
    public String toString() {
        return "(" + x + "; " + y + "; " + z + ")";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Vec3 vector = (Vec3) obj;
        return Float.compare(vector.x, x) == 0 &&
                Float.compare(vector.y, y) == 0 &&
                Float.compare(vector.z, z) == 0;
    }

    @Override
    public Vec3 clone() { return new Vec3(this); }

}
