package math;

@FunctionalInterface
public interface FunTimePosSpeed {

    /**
     * Typical form of a second order differencial equation
     * @param t     (float) time
     * @param pos   (Vec3)   position at time t
     * @param speed (Vec3)   speed at time t
     * @return      (Vec3)   acceleration
     */
    Vec3 apply(float t, Vec3 pos, Vec3 speed);


    /**
     * Adds the functions passed as arguments together
     * @param functions (Elapse of functions) functions to add together
     * @return (FunTimePosSpeed) the addition of the functions
     */
    static FunTimePosSpeed add(FunTimePosSpeed... functions) {
        return (float t, Vec3 pos, Vec3 speed) -> {
            Vec3 temp = new Vec3();
            for (FunTimePosSpeed func : functions) {
                temp = Vec3.add(func.apply(t, pos, speed), temp);
            }
            return temp;
        };
    }

    /**
     * Scales the equation by a float scalar
     * @param constant (float) value to scale with
     * @return (FunTimePosSpeed) result
     */
    default FunTimePosSpeed scale(float constant) {
        return (float t, Vec3 position, Vec3 speed) -> Vec3.scale(constant, this.apply(t, position, speed));
    }

}
