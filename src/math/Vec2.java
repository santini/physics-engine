package math;

public class Vec2 {

    private static final float EPSILON = 0.001f;
    private float x;
    private float y;

    // ===============================================================
    // ========================= CONSTRUCTORS ========================
    // ===============================================================

    /**
     * Constructor for Vec2
     * @param x (float) x-Coordinate
     * @param y (float) y-Coordinate
     */
    public Vec2(float x, float y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Copy constructor for Vec2
     * @param other (Vec2) Vector to copy
     */
    public Vec2(Vec2 other) { this(other.x, other.y);}

    /**
     * Default constructor for Vec2, creates a null vector
     */
    public Vec2() { this(0, 0); }


    // ===============================================================
    // ========================= OPERATORS ===========================
    // ===============================================================


    /**
     * Adds another vector to the instance
     * @param other (Vec2) vector to add
     * @return (Vec2) The modified instance
     */
    public Vec2 addLocal(Vec2 other) {
        x += other.x;
        y += other.y;
        return this;
    }

    /**
     * Adds two given values to the vector components
     * @param dx (float) value to add to x
     * @param dy (float) value to add to y
     * @return (Vec2) The instance of the modified vector
     */
    public Vec2 addLocal(float dx, float dy) {
        x += dx;
        y += dy;
        return this;
    }

    /**
     * Scales the instance locally
     * @param scalar (float) Scalar to scale with
     * @return (Vec2) The modified instance
     */
    public Vec2 scaleLocal(float scalar) {
        x *= scalar;
        y *= scalar;
        return this;
    }


    /**
     * Returns the sum of both vectors
     * @param v1 (Vec2) first vector
     * @param v2 (Vec2) second vector
     * @return (Vec2) Result of the operation
     */
    public static Vec2 add(Vec2 v1, Vec2 v2) {
        return new Vec2(v1.x + v2.x, v1.y + v2.y); }


    /**
     * Returns the subtraction of both vectors
     * @param v1 (Vec2) first vector
     * @param v2 (Vec2) second vector
     * @return (Vec2) Result of the operation
     */
    public static Vec2 sub(Vec2 v1, Vec2 v2) {
        return new Vec2(v1.x - v2.x, v1.y - v2.y); }


    /**
     * Returns the scaled vector
     * @param scalar (float) Scalar
     * @param vec (Vec2) Vector to scale
     * @return (Vec2) Result of the operation
     */
    public static Vec2 scale(float scalar, Vec2 vec) {
        return new Vec2(vec.x * scalar, vec.y * scalar); }


    /**
     * Returns the first vector scaled added to the second one
     * @param s (float) Scalar for the first vector
     * @param vec1 (Vec2) First vector
     * @param vec2 (Vec2) Second vector
     * @return (Vec2) Result of the operation
     */
    public static Vec2 fma(float s, Vec2 vec1, Vec2 vec2) {
        return Vec2.add(Vec2.scale(s, vec1), vec2); }


    /**
     * Returns the dot product between the two vectors
     * @param v1 (Vec2) First vector
     * @param v2 (Vec2) Second vector
     * @return (float) Dot product of both vectors
     */
    public static float dot(Vec2 v1, Vec2 v2) {
        return v1.x*v2.x + v1.y*v2.y;
    }


    // ===============================================================
    // ========================== METHODS ============================
    // ===============================================================

    public float squaredNorm() { return x*x + y*y; }

    public float norm() { return (float) Math.sqrt(squaredNorm()); }

    public void normalize() { this.scaleLocal(1/norm()); }

    public Vec2 normalized() { return Vec2.scale(1f/this.norm(), new Vec2(this));}

    public boolean isNull() { return norm()<EPSILON; }

    public float[] toArray() { return new float[]{x, y}; }

    public Vec2 negate() { return new Vec2(-x, -y); }

    // ===============================================================
    // ========================= GETTERS =============================
    // ===============================================================

    public float x() { return x; }
    public float y() { return y; }

    // ===============================================================
    // ========================= OVERRIDES ===========================
    // ===============================================================

    @Override
    public String toString() {
        return "(" + x + "; " + y + ")";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Vec2 vector = (Vec2) obj;
        return Float.compare(vector.x, x) == 0 &&
                Float.compare(vector.y, y) == 0;
    }

    @Override
    public Vec2 clone() { return new Vec2(this); }

}
