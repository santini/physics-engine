package math.Integrators;

import engine.Projectile;
import math.FunTimePosSpeed;
import math.Vec3;

public class RK4 extends Integrateur {

    public RK4(Projectile obj) { super(obj); }


    /**
     * Implementation of Runge-Kunta 4th order integrator for movement
     * of physical objects linked to an equation
     * @param f  (FunTimePosSpeed) differential equation of the object
     * @param t  (float)           time in engine
     * @param dt (float)           interval of time elapsed in engine
     */

    @Override
    public void move(FunTimePosSpeed f, float t, float dt) {

        Vec3 pos = obj.getPosition();
        Vec3 speed = obj.getSpeed();
        Vec3 k1 = speed;
        Vec3 k1_ = f.apply(t, pos, speed);
        Vec3 k2 = Vec3.fma(dt/2, k1_, speed);
        Vec3 k2_ = f.apply(t + dt/2, Vec3.fma(dt/2, k1, pos), Vec3.fma(dt/2, k1, speed));
        Vec3 k3 = Vec3.fma(dt/2, k2_, speed);
        Vec3 k3_ = f.apply(t + dt/2, Vec3.fma(dt/2, k2, pos), Vec3.fma(dt/2, k2, speed));
        Vec3 k4 = Vec3.fma(dt, k3_, speed);
        Vec3 k4_ = f.apply(t + dt, Vec3.fma(dt, k3, pos), Vec3.fma(dt, k3, speed));

        obj.setPosition(Vec3.fma(dt/6, sum(k1, Vec3.scale(2, k2), Vec3.scale(2, k3), k4), pos));
        obj.setSpeed(Vec3.fma(dt/6, sum(k1_, Vec3.scale(2, k2_), Vec3.scale(2, k3_), k4_), speed));

    }

    private Vec3 sum(Vec3... vecs) {
        Vec3 temp = new Vec3();
        for (Vec3 vec : vecs)
            temp = Vec3.add(temp, vec);

        return temp;
    }


}
