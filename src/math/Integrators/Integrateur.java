package math.Integrators;

import engine.Projectile;
import math.FunTimePosSpeed;

public abstract class Integrateur {

    protected Projectile obj;

    public Integrateur(Projectile obj) {
        this.obj = obj;
    }

    public abstract void move(FunTimePosSpeed f, float t, float dt);

}
