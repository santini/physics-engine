package renderer.objects;

import javafx.scene.paint.Color;
import math.Vec3;
import renderer.ObjWrapper;

/**
 * 3D Plane
 */
public class Plane implements Shape {
    private Vec3 position;
    private final Vec3 normal;
    private final Color color;

    /**
     * Plane constructor
     * @param position (Vec3) Position of a point belonging to the plane
     * @param normal (Vec3) Normal vector of the plane (direction matters)
     * @param color (Color) The color of the shape
     */
    public Plane(Vec3 position, Vec3 normal, Color color) {
        if (normal.isNull()) throw new IllegalArgumentException("Null vector passed as normal.");
        this.position = position;
        this.normal = normal;
        this.color = color;
    }

    /**
     * Plane constructor
     * @param position (Vec3) Position of a point belonging to the plane
     * @param b1 (Vec3) First vector lying in the plane
     * @param b2 (Vec3) Second vector lying in the plane (non-collinear to the first)
     * @param color (Color) The color of the shape
     */
    public Plane(Vec3 position, Vec3 b1, Vec3 b2, Color color) {
        this(position, Vec3.cross(b1, b2), color);
    }

    /**
     * Getter for the position of the plane
     * @return (Vec3) Position of a point of the plane in 3D
     */
    @Override
    public Vec3 getPosition() {
        return new Vec3(position);
    }

    /**
     * Setter for the position of the Plane
     * @param newPos (Vec3) new position
     */
    @Override
    public void setPosition(Vec3 newPos) { position = newPos.clone(); }

    /**
     * Getter for the color of the plane
     * @return (float[]) float array containing RGB values of the color [0, 1]
     */
    @Override
    public float[] getColor() {
        return new float[]{(float) color.getRed(), (float) color.getGreen(), (float) color.getBlue()};
    }

    /**
     * Getter for the ID of the plane
     * @return ObjWrapper.Types.PLANE
     */
    @Override
    public ObjWrapper.Types getCode() {
        return ObjWrapper.Types.PLANE;
    }

    /**
     * Getter for the attributes of a plane
     * @return (float[]) The normal of the plane in a float array
     */
    @Override
    public float[] getAttributes() {
        return normal.toArray();
    }
}
