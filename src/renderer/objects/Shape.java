package renderer.objects;

import math.Vec3;
import renderer.ObjWrapper;

/**
 * Representation of an object drawable by the renderer
 */
public interface Shape {
    /**
     Getter for the position of the object in the scene
     @return (Vec3) position in 3D space
     */
    Vec3 getPosition();

    /**
     * Setter for the position of the shape
     * @param vec (Vec3) new position
     */
    void setPosition(Vec3 vec);

    /**
     * Getter for the color of the object
     * @return (flaot[]) float array containing RGB values [0, 1]
     */
    float[] getColor();

    /**
     * Getter for the ID of the object (can be obtained via ObjWrapper.Types)
     * @return (ObjWrapper.Type) ID for the type of object
     */
    ObjWrapper.Types getCode();

    /**
     * Geometric attributes of the shape
     * @return (float[]) pre-formated array of attributes
     */
    float[] getAttributes();

}
