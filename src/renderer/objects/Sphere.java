package renderer.objects;

import javafx.scene.paint.Color;
import math.Vec3;
import renderer.ObjWrapper;

/**
 * 3D Sphere
 */
public class Sphere implements Shape {
    private Vec3 position;
    private final float radius;
    private final Color color;

    /**
     * Constructor for a sphere
     * @param position (Vec3) Position of the sphere in 3D
     * @param radius (float) Radius of the sphere
     * @param color (Color) Color of the sphere
     */
    public Sphere(Vec3 position, float radius, Color color) {
        this.position = position;
        this.radius = radius;
        this.color = color;
    }

    public void setPosition(Vec3 pos) {
        position = new Vec3(pos);
    }

    /**
     *  Getter for the position of the sphere in 3D
     * @return (Vec3) Position of the sphere in the scene
     */
    @Override
    public Vec3 getPosition() { return new Vec3(position); }

    /**
     * Getter for the color of the sphere
     * @return (float[]) A float array containing the RGB values [0, 1]
     */
    @Override
    public float[] getColor() {
        return new float[]{(float) color.getRed(), (float) color.getGreen(), (float) color.getBlue()};
    }

    /**
     * Getter for the ID of the sphere
     * @return ObjWrapper.Types.SPHERE
     */
    @Override
    public ObjWrapper.Types getCode() {
        return ObjWrapper.Types.SPHERE;
    }

    /**
     * Getter for the geometric attributes of the sphere
     * @return (float[]) Radius of the sphere
     */
    @Override
    public float[] getAttributes() {
        return new float[]{radius};
    }
}
