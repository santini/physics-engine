package renderer;

import javafx.scene.image.ImageView;
import javafx.scene.image.PixelBuffer;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.WritableImage;
import org.jocl.*;
import renderer.objects.Shape;
import renderer.setup.DeviceMgmt;
import renderer.setup.IO;

import java.io.IOException;
import java.nio.IntBuffer;
import java.util.HashSet;
import java.util.Set;

public class Renderer {

    private int imageHeight;
    private int imageWidth;
    private int totNbOfPixels;

    private final PixelBuffer<IntBuffer> pixelBuffer;
    private final IntBuffer imageBuffer;
    private final Pointer bufferPointer;
    private final ImageView canvas;
    private final Camera camera;

    private final Set<Shape> drawables;


    private int[] dimensions;
    private final long[] global_work_size;

    private final cl_context CONTEXT;
    private final cl_command_queue COMMAND_QUEUE;
    private cl_kernel kernel;
    private final cl_mem[] memObjects = new cl_mem[5];

    private final cl_event imageCreating;
    private final cl_event readingBuffer;
    private boolean running = true;


    /**
     * Constructor for the renderer
     * @param imageHeight (int) initial image height in pixels
     * @param imageWidth (int) initial image width in pixels
     */
    public Renderer(int imageHeight, int imageWidth, Camera camera, ImageView canvas, Set<Shape> shapeList) {
        this.imageHeight = imageHeight;
        this.imageWidth = imageWidth;
        totNbOfPixels = imageHeight*imageWidth;

        imageBuffer = IntBuffer.allocate(totNbOfPixels);
        pixelBuffer = new PixelBuffer<>(imageWidth, imageHeight, imageBuffer, PixelFormat.getIntArgbPreInstance());
        bufferPointer = Pointer.to(imageBuffer);

        this.camera = camera;
        this.canvas = canvas;

        drawables = shapeList;

        dimensions = new int[] {imageHeight, imageWidth};
        // Set the work-item dimensions
        global_work_size = new long[]{imageHeight, imageWidth};

        DeviceMgmt.Info platformInfo = DeviceMgmt.initialize();
        CONTEXT = platformInfo.context();
        COMMAND_QUEUE = platformInfo.commandQueue();
        imageCreating = CL.clCreateUserEvent(CONTEXT, null);
        readingBuffer = CL.clCreateUserEvent(CONTEXT, null);

        try {
            kernel = IO.getKernel(CONTEXT, "engine_kernel.cl");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Renderer(int imageHeight, int imageWidth, Camera camera, ImageView canvas) {
        this(imageHeight, imageWidth, camera, canvas, new HashSet<>());}

    /**
     * Initiates the "reading buffer" event to allow the launch of the main loop
     */
    public void start() {
        // Allocate the memory objects for the input and output data
        memObjects[0] = CL.clCreateBuffer(CONTEXT,
                CL.CL_MEM_WRITE_ONLY, (long) totNbOfPixels * Sizeof.cl_float, null, null);
        memObjects[1] = CL.clCreateBuffer(CONTEXT,
                CL.CL_MEM_USE_HOST_PTR | CL.CL_MEM_READ_ONLY, Sizeof.cl_int*2, Pointer.to(new int[]{1}), null);
        memObjects[2] = CL.clCreateBuffer(CONTEXT,
                CL.CL_MEM_USE_HOST_PTR, Sizeof.cl_int, Pointer.to(new int[]{1}), null);
        memObjects[3] = CL.clCreateBuffer(CONTEXT,
                CL.CL_MEM_USE_HOST_PTR | CL.CL_MEM_READ_ONLY, Sizeof.cl_int, Pointer.to(new int[]{1}), null);
        memObjects[4] = CL.clCreateBuffer(CONTEXT,
                CL.CL_MEM_USE_HOST_PTR | CL.CL_MEM_READ_ONLY, Sizeof.cl_float, Pointer.to(new float[]{1}), null);


        CL.clEnqueueReadBuffer(COMMAND_QUEUE, memObjects[0], CL.CL_TRUE, 0,
                (long) totNbOfPixels * Sizeof.cl_float, Pointer.to(new int[totNbOfPixels]), 0, null, readingBuffer);

    }

    /**
     * Renders the scene with the given shapes
     */
    public void render() {

        if (!running) {
            CL.clWaitForEvents(2, new cl_event[]{imageCreating, readingBuffer});
            imageBuffer.clear();
            CL.clReleaseEvent(imageCreating);
            CL.clReleaseEvent(readingBuffer);
            CL.clReleaseKernel(kernel);
            CL.clReleaseCommandQueue(COMMAND_QUEUE);
            CL.clReleaseContext(CONTEXT);
            return;
        }

        float[] objsBuffer = ObjWrapper.wrap(drawables);

        // Read the output data
        CL.clEnqueueReadBuffer(COMMAND_QUEUE, memObjects[0], CL.CL_TRUE, 0,
                (long) totNbOfPixels * Sizeof.cl_float, bufferPointer, 0, null, readingBuffer);

        WritableImage wr = new WritableImage(pixelBuffer);
        canvas.setImage(wr);

        // Release memory objects
        for (cl_mem memObject : memObjects)
            CL.clReleaseMemObject(memObject);


        // Allocate the memory objects for the input and output data
        memObjects[0] = CL.clCreateBuffer(CONTEXT,
                CL.CL_MEM_WRITE_ONLY, (long) Sizeof.cl_float * totNbOfPixels, null, null);
        memObjects[1] = CL.clCreateBuffer(CONTEXT,
                CL.CL_MEM_USE_HOST_PTR | CL.CL_MEM_READ_ONLY, Sizeof.cl_int*2, Pointer.to(dimensions), null);
        memObjects[2] = CL.clCreateBuffer(CONTEXT,
                CL.CL_MEM_USE_HOST_PTR, Sizeof.cl_float*3, Pointer.to(camera.getPosition().toArray()), null);
        memObjects[3] = CL.clCreateBuffer(CONTEXT,
                CL.CL_MEM_USE_HOST_PTR | CL.CL_MEM_READ_ONLY, Sizeof.cl_float*3, Pointer.to(camera.getAim().toArray()), null);
        memObjects[4] = CL.clCreateBuffer(CONTEXT,
                CL.CL_MEM_USE_HOST_PTR | CL.CL_MEM_READ_ONLY, (long) Sizeof.cl_float*objsBuffer.length, Pointer.to(objsBuffer), null);


        for (int i=0; i< memObjects.length; ++i)
            CL.clSetKernelArg(kernel, i, Sizeof.cl_mem, Pointer.to(memObjects[i]));


        // Execute the kernel
        CL.clEnqueueNDRangeKernel(COMMAND_QUEUE, kernel, 2, null,
                global_work_size, null, 0, null, imageCreating);


    }

    public void addDrawable(Shape obj) { drawables.add(obj); }
    public void removeDrawable(Shape obj) { drawables.remove(obj); }
    public void clearDrawables() { drawables.clear(); }

    /**
     * Updates the kernel
     */
    public void updateKernel() {
        try {
            kernel = IO.getKernel(CONTEXT, "main " + "_kernel.cl");
        } catch (IOException e) {
            throw new RuntimeException("Kernel file could not be found");
        } catch (CLException e) {
            throw new RuntimeException("Kernel could not compile, please check for mistakes");
        }
    }

    /**
     * Used to resize the image
     * @param newHeight (double) new height of the image in pixels
     * @param newWidth (double) new width of the image in pixels
     */
    public void resize(double newHeight, double newWidth) {
        imageHeight = (int) newHeight;
        imageWidth = (int) newWidth;
        totNbOfPixels = imageHeight*imageWidth;
        dimensions = new int[]{imageHeight, imageWidth};
    }

    /**
     * Releases the kernel, command_queue and context
     */
    public void end() { running = false; }


}
