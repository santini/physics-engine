package renderer.setup;

import org.jocl.CL;
import org.jocl.cl_context;
import org.jocl.cl_kernel;
import org.jocl.cl_program;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;

public class IO {

    /**
     * Writes the given image to the disk
     * @param path (String) Path and name of the image
     * @param imageBuffer (int[]) buffered image
     * @param height (int) image height
     * @param width (int) image length
     */
    public static void writeImage(String path, int[] imageBuffer, int height, int width) {
        int type = BufferedImage.TYPE_4BYTE_ABGR;
        BufferedImage buffer = new BufferedImage(width, height, type);

        for(int i = 0; i < buffer.getHeight(); ++i)
            for(int j = 0 ; j < buffer.getWidth(); ++j)
                buffer.setRGB(j, height-1-i, imageBuffer[i*width+j]);

        try {
            ImageIO.write(buffer, "png", new File(path));
        } catch (IOException e) {
            System.out.println("Failed to write to given folder");
        }

    }

    /**
     * Gets the kernel from disk and compiles it
     * @param context (cl_context) context to which the kernel will be linked to
     * @param fileName (String) name of the kernel to use (picked in the kernels/ folder)
     * @return (cl_kernel) usable kernel
     * @throws IOException if the file cannot be found
     */
    public static cl_kernel getKernel(cl_context context, String fileName) throws IOException {
        BufferedReader reader;
        StringBuilder code = new StringBuilder();
        String kernelName;

        reader = new BufferedReader(new FileReader("kernels/"+fileName));

        String line = reader.readLine();
        kernelName = line.replace("//", "").replace(" ", "");

        line = reader.readLine();
        while (line != null) {
            code.append(line).append("\n");
            line = reader.readLine();
        }

        reader.close();



        // Create the program from the source code
        cl_program program = CL.clCreateProgramWithSource(context,
                1, new String[]{ code.toString() }, null, null);

        // Build the program
        CL.clBuildProgram(program, 0, null,
                "-I " +  System.getProperty("user.dir") + File.separator + "kernels", null, null);
        cl_kernel kernel = CL.clCreateKernel(program, kernelName, null);

        CL.clReleaseProgram(program);

        return kernel;
    }


}
