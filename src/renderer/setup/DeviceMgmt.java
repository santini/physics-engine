package renderer.setup;

import org.jocl.*;

/**
 * Manager for the device used to render
 */
public class DeviceMgmt {

    /**
     * Record containing the context and command queue necessary for the render
     * @param context (cl_context) Context for the render
     * @param commandQueue (cl_command_queue) Command queue for running the kernel
     */
    public record Info(cl_context context, cl_command_queue commandQueue) {}

    /**
     * Initializes the device and gets the command queue and context for render
     * @return (Info) record containing the command queue and context
     */
    public static Info initialize() {
        // The platform, device type and device number
        // that will be used
        final int platformIndex = 0;
        final int deviceIndex = 0;

        // Enable exceptions and subsequently omit error checks in this sample
        CL.setExceptionsEnabled(false);

        // Obtain the number of platforms
        int[] numPlatformsArray = new int[1];
        CL.clGetPlatformIDs(0, null, numPlatformsArray);
        int numPlatforms = numPlatformsArray[0];

        // Obtain a platform ID
        cl_platform_id[] platforms = new cl_platform_id[numPlatforms];
        CL.clGetPlatformIDs(platforms.length, platforms, null);
        cl_platform_id platform = platforms[platformIndex];

        // Initialize the context properties
        cl_context_properties contextProperties = new cl_context_properties();
        contextProperties.addProperty(CL.CL_CONTEXT_PLATFORM, platform);

        // Obtain the number of devices for the platform
        int[] numDevicesArray = new int[1];
        CL.clGetDeviceIDs(platform, CL.CL_DEVICE_TYPE_GPU, 0, null, numDevicesArray);
        int numDevices = numDevicesArray[0];

        // Obtain a device ID
        cl_device_id[] devices = new cl_device_id[numDevices];
        CL.clGetDeviceIDs(platform, CL.CL_DEVICE_TYPE_GPU, numDevices, devices, null);

        cl_device_id device = devices[deviceIndex];

        // Create a context for the selected device
        cl_context context = CL.clCreateContext(
                contextProperties, 1, new cl_device_id[]{device},
                null, null, null);

        // Create a command-queue for the selected device
        cl_command_queue commandQueue =
                CL.clCreateCommandQueueWithProperties(context, device, null, null);

        System.out.println("Device detected : " + getString(device, CL.CL_DEVICE_NAME));
        System.out.println("Number of compute units available : " + getString(device, CL.CL_DEVICE_MAX_COMPUTE_UNITS));
        System.out.println("Max work item per work group : " + getString(device, CL.CL_DEVICE_MAX_WORK_GROUP_SIZE));
        System.out.println("Max clock speed of gpu (too flex a little) : " + getString(device, CL.CL_DEVICE_MAX_CLOCK_FREQUENCY));

        CL.clReleaseDevice(device);

        return new Info(context, commandQueue);
    }


    /**
     * Getter for device information
     * @param device (cl_device_id) id of the desired device
     * @param paramName (int) Code corresponding to information to get on the device
     * @return (String) information
     */
    private static String getString(cl_device_id device, int paramName) {
        // Obtain the length of the string that will be queried
        long[] size = new long[1];
        CL.clGetDeviceInfo(device, paramName, 0, null, size);

        switch (paramName) {
            case CL.CL_DEVICE_NAME -> {
                byte[] buffer = new byte[(int) size[0]];
                CL.clGetDeviceInfo(device, paramName, buffer.length, Pointer.to(buffer), null);
                // Create a string from the buffer (excluding the trailing \0 byte)
                return new String(buffer, 0, buffer.length - 1);
            }
            case CL.CL_DEVICE_MAX_COMPUTE_UNITS, CL.CL_DEVICE_MAX_WORK_GROUP_SIZE, CL.CL_DEVICE_MAX_CLOCK_FREQUENCY -> {
                int[] val = new int[1];
                CL.clGetDeviceInfo(device, paramName, size[0], Pointer.to(val), null);
                return Integer.toString(val[0]);
            }
            default -> {
                return "Queried info : " + CL.stringFor_cl_device_info(paramName) + " not compatible.";}
        }

    }

}
