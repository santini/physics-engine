package renderer;

import math.Vec3;

/**
 * Camera class capable of moving in 3D space
 */
public class Camera {
    /** Up vector */
    private final static Vec3 UP = new Vec3(0, 0, 1);

    /** Movement speed of the camera */
    private final float mvmtSpeed = 10f;

    /** Rotation speed of the camera */
    private final float rotSpeed = 0.1f;

    /** Position of the camera in 3D space */
    private final Vec3 position;

    /** Direction the camera is pointing in */
    private final Vec3 aim;

    /**
     * Constructor for the camera
     * @param position (Vec3) Original position of the camera
     * @param aim (Vec3) Original aim of the camera
     */
    public Camera(Vec3 position, Vec3 aim) {
        this.position = position;
        this.aim = aim;
    }


    // ==========================================================
    // ======================== MOVEMENTS =======================
    // ==========================================================

    /**
     * Turns the camera following the horizontal and vertical angles
     * @param hor (float) Angle of rotation along the XY plane in radians
     * @param vert (float) Angle of rotation along the vertical plane in radians
     * @param dt (float) Time step since last update
     */
    public void turn(float hor, float vert, float dt) {
        aim.rotate(rotSpeed * hor * dt, rotSpeed * vert * dt);
    }

    /**
     * Moves the camera upwards
     * @param dt (float) Time step since last update
     */
    public void moveUp(float dt) { position.addLocal(new Vec3(0, 0, mvmtSpeed * dt)); }

    /**
     * Moves the camera downwards
     * @param dt (float) Time step since last update
     */
    public void moveDown(float dt) { position.addLocal(new Vec3(0, 0, -mvmtSpeed * dt)); }

    /**
     * Moves the camera forwards
     * @param dt (float) Time step since last update
     */
    public void moveForwards(float dt) { position.addLocal(Vec3.scale(mvmtSpeed * dt, aim)); }

    /**
     * Moves the camera backwards
     * @param dt (float) Time step since last update
     */
    public void moveBackwards(float dt) { position.addLocal(Vec3.scale(-mvmtSpeed * dt, aim)); }

    /**
     * Moves the camera left
     * @param dt (float) Time step since last update
     */
    public void moveLeft(float dt) {
        position.addLocal(
                Vec3.cross(UP, aim).scaleLocal(mvmtSpeed * dt)
        );
    }

    /**
     * Moves the camera right
     * @param dt (float) Time step since last update
     */
    public void moveRight(float dt) {
        position.addLocal(
                Vec3.cross(UP, aim).scaleLocal(-mvmtSpeed * dt)
        );
    }

    // ==========================================================
    // ====================== GETTERS ===========================
    // ==========================================================

    /**
     * Getter for the camera's position
     * @return (Vec3) A clone of the camera's position vector
     */
    public Vec3 getPosition() { return position.clone(); }

    /**
     * Getter for the camera's aim
     * @return (Vec3) A clone of the camera's aim
     */
    public Vec3 getAim() { return aim.clone(); }

}
