package renderer;

import renderer.objects.Shape;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;

/**
 * Class for wrapping the objects to a float array readable by the GPU
 */
public class ObjWrapper {

    /**
     * Enum containing the base information for each shape
     */
    public enum Types {
        SPHERE(8), PLANE(10);

        public final int NB_OF_ATTRIBUTES; // Number of attributes the kernel will have to read
        Types(int nbAtr) { NB_OF_ATTRIBUTES = nbAtr; }

        public String getName() {
            return switch (ordinal()) {
                case 0 -> "SPHERE";
                case 1 -> "PLANE";
                default -> throw new IllegalStateException("Unexpected value: " + ordinal());
            };
        }

    }

    /**
     * Wraps the Shapes to a float array
     * @param objList (ArrayList(Shape)) list of objects to wrap
     * @return (float[]) the float array of encoded objects
     */
    public static float[] wrap(Set<Shape> objList) {
        ArrayList<Float> buffer = new ArrayList<>();

        for (Shape obj : objList) {
            switch (obj.getCode()) {

                case SPHERE -> {
                    buffer.add((float) Types.SPHERE.ordinal());

                    addPosCol(buffer, obj);

                    float[] atr = obj.getAttributes();
                    if (atr.length != 1)
                        throw new IllegalArgumentException
                                ("The attributes for the sphere do not correspond to the official quantity.");

                    buffer.add(atr[0]);


                 }

                case PLANE -> {
                    buffer.add((float) Types.PLANE.ordinal());

                    addPosCol(buffer, obj);

                    float[] atr = obj.getAttributes();
                    if (atr.length != 3)
                        throw new IllegalArgumentException
                                ("The attributes for the plane do not correspond to the official quantity.");

                    buffer.add(atr[0]);
                    buffer.add(atr[1]);
                    buffer.add(atr[2]);

                }
            }

        }

        buffer.add(0, (float) (buffer.size()));

        // TODO find more efficient
        float[] temp = new float[buffer.size()];
        for (int i = 0; i<buffer.size(); ++i)
            temp[i] = buffer.get(i);

        return temp;
    }

    /**
     * Used for adding the position and the color of the shape to the buffer
     * @param buffer (ArrayList(Float)) Buffer to write to
     * @param obj (Shape) Shape to add
     */
    private static void addPosCol(ArrayList<Float> buffer, Shape obj) {
        float[] pos = obj.getPosition().toArray();
        buffer.add(pos[0]);
        buffer.add(pos[1]);
        buffer.add(pos[2]);

        float[] col = obj.getColor();
        buffer.add(col[0]);
        buffer.add(col[1]);
        buffer.add(col[2]);
    }


    /**
     * Creates the header file containing the codes for extracting data of the buffer
     */
    public static void createEncodeRefFile() {
        try {
            BufferedWriter br = new BufferedWriter(new FileWriter("kernels/constants.h"));
            for (Types type : Types.values()) {
                br.write("#define " + type.getName() + " " + type.ordinal());
                br.newLine();
                br.write("#define " + type.getName() + "_ATR_LENGTH " + type.NB_OF_ATTRIBUTES);
                br.newLine();
            }
            br.close();

        } catch (IOException e) {
            System.out.println("Failed to write encoding constants :");
            e.printStackTrace();
        }
    }


}
