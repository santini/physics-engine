# SIMPLE PHYSICS ENGINE

## Setup :

It is required that you have Java FX setup and that you have linked 
the OpenCL bindings (JOCL) from https://repo1.maven.org/maven2/org/jocl/jocl/2.0.5/jocl-2.0.5.jar

## Demonstration :

Here's an example of the program with two objects orbiting one another :

![](res/readme/orbit_example.gif)

## TODO
- Simplify project architecture
- Implement collisions
- Improve serialization for objects
