// mkernel

#include "constants.h"

#define ZOOM      1

#define MAX_STEPS 200
#define MAX_DIST  100.
#define EPSILON   .01f


bool compare(float a, float b) { return (fabs(a - b)<0.001); }

int toInt(float3 pixel) {
    int pixelValue = 0;

    char3 finalPixel = 0;

    for(int i = 0; i < 3; ++i)
        finalPixel[i] = (uchar)(pixel[i]*255);

    pixelValue |= 0b011111111 & 255;
    pixelValue = pixelValue << 8;

    pixelValue |= 0b011111111 & finalPixel.x;
    pixelValue = pixelValue << 8;

    pixelValue |= 0b011111111 & finalPixel.y;
    pixelValue = pixelValue << 8;

    pixelValue |= 0b011111111 & finalPixel.z;

    return pixelValue;
}

float3 GetRayDirection(float3 aim, float2 uv) {
        float3 up = {0, 0, 1};
        float3 bx = normalize(cross(aim, up));
        //float3 by = cross(bx, aim);
        float3 angling = uv.x*bx + uv.y*cross(bx, aim) ;

        return normalize(aim*ZOOM+angling);
}

float getDistSphere(float3 pos, float4 sphere) {
    return length(pos-sphere.xyz) - sphere.w;
}

float4 minDist(float4 a, float4 b) {
    return a.x<b.x ? a : b;
}

float getDistPlane(float3 pos, float8 plane) {
    return dot(pos, normalize(plane.s345)) + length(plane.xyz);
}


float4 getDistCol(float3 position, __constant float* objects) {
    int arr_length = objects[0];

    float4 distCol = {INFINITY, 0, 0, 0};

    int index = 1;
    while (index < arr_length) {
        float objType = objects[index];

        if (compare(objType, SPHERE)) {
            float4 sphere = {objects[index+1], objects[index+2], objects[index+3], objects[index+7]};
            float4 temp = { getDistSphere(position, sphere), objects[index+4], objects[index+5], objects[index+6]};
            
            distCol = minDist(distCol, temp);

            index += SPHERE_ATR_LENGTH;

        } else if (compare(objType, PLANE)) {
            float8 plane = {objects[index+1], objects[index+2], objects[index+3],         // Position
                            objects[index+7], objects[index+8], objects[index+9], 0, 0};  // Normal (+filler 0s)
            float4 temp = { getDistPlane(position, plane), objects[index+4], objects[index+5], objects[index+6]};
            
            distCol = minDist(distCol, temp);

            index += PLANE_ATR_LENGTH;

        } else {
            float4 errCol = {500, 1, 0, 0};
            return errCol;
        }

    }

    return distCol;

}

float getDist(float3 position, __constant float* objects) {
    return getDistCol(position, objects).x;
}


float4 RayMarch(float3 ro, float3 rd, __constant float* objects) {
    float d0 = 0.;
    float4 dS;

    for (int i=0; i<MAX_STEPS; ++i ) {
        float3 position = ro + rd*d0;
        dS = getDistCol(position, objects);
        d0+=dS.x;
        if (d0>MAX_DIST || dS.x<EPSILON) break;
    }

    dS.x = d0;
    return dS;
}


float3 GetNormal(float3 pos, __constant float* objects) {
    float d = getDist(pos, objects);
    float2 e = {.01, 0};

    float3 temp = {getDist(pos-e.xyy, objects),
                   getDist(pos-e.yxy, objects),
                   getDist(pos-e.yyx, objects)};

    return normalize(d - temp);
}

float GetLight(float3 pos, __constant float* objects) {
    float3 lightPos = {0, -50, 0};
    float3 l = normalize(lightPos - pos);
    float3 n = GetNormal(pos, objects);


    float diff = clamp(dot(n, l), 0.f, 1.f);
    float d = RayMarch(pos + 2*EPSILON*n, l, objects).x;

    if (d < length(lightPos-pos)) diff *= 0.2; // For shade

    return diff;
}


__kernel void mkernel(__global int *image, __constant int *dimension,
                      __constant float *position, __constant float *aimDir, __constant float *objs) {

    float2 res = {dimension[0], dimension[1]};

    int idx = get_group_id(0)*get_local_size(0) + get_local_id(0);
    int idy = get_group_id(1)*get_local_size(1) + get_local_id(1);

    if ((idx >= res.x) || (idy >= res.y)) return;

    float2 uv = {idy, idx};
    uv = (uv - (res*0.5f))/res.y;


    float3 col = 0;

    float3 aim = { aimDir[0], aimDir[1], aimDir[2]};
    aim = normalize(aim);

    float3 rd = GetRayDirection(aim, uv);

    // Origin of the camera
    float3 ro = {position[0], position[1], position[2]};

    float4 distCol = RayMarch(ro, rd, objs);
    float3 impact = rd*distCol.x + ro;

    col = distCol.yzw * GetLight(impact, objs);

    image[dimension[0]*dimension[1] - idx*dimension[1]+idy] = toInt(col);

}

