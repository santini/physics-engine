# Quelques précisions sur l'implémentation de OpenCL en Java :

 * ## Lecture et compilation du compute shader:
    Le compute shader (main_kernel.cl) est lu par une méthode de IO : `getKernel(cl_context context, String path)` qui utilise 
    un BufferedReader. Il est important que la première ligne du shader contienne en commentaire UNIQUEMENT le nom du kernel
    à appeller, puisque le nom est récupéré et utilisé dans getKernel().
 * ## Passage d'arguments:
   Le kernel du compute shader reçoit en arguments :
   - Le buffer de pixels a remplir
   - Les dimmensions de l'image
   - La position de la camera
   - La direction de la camera
   - Les objects de la scene serialisés par `ObjWrapper`